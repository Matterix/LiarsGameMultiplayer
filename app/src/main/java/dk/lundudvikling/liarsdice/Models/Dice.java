package dk.lundudvikling.liarsdice.Models;


public class Dice {

    public int diceId;
    public int diceValue;

    public Dice() {
    }

    public Dice(int diceId, int diceValue) {
        this.diceId = diceId;
        this.diceValue = diceValue;
    }

    public int getDiceId() {
        return diceId;
    }

    public void setDiceId(int diceId) {
        this.diceId = diceId;
    }

    public int getDiceValue() {
        return diceValue;
    }

    public void setDiceValue(int diceValue) {
        this.diceValue = diceValue;
    }
}
