package dk.lundudvikling.liarsdice.Models;


import java.util.ArrayList;

public class Player {

    public String name;
    public String playerNumber;

    public String getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(String playerNumber) {
        this.playerNumber = playerNumber;
    }

    public ArrayList<Dice> playerDice;

    public Player() {
    }

    public Player(String name, ArrayList<Dice> playerDice, String playerNumber) {
        this.name = name;
        this.playerDice = playerDice;
        this.playerNumber = playerNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<Dice> getPlayerDice() {
        return playerDice;
    }

    public void setPlayerDice(ArrayList<Dice> playerDice) {
        this.playerDice = playerDice;
    }
}
