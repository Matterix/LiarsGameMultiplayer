package dk.lundudvikling.liarsdice;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Random;


import dk.lundudvikling.liarsdice.Fragments.CreateGameFragment;
import dk.lundudvikling.liarsdice.Fragments.JoinGameFragment;
import dk.lundudvikling.liarsdice.Fragments.MainFragment;
import dk.lundudvikling.liarsdice.Handlers.FragmentHandler;
import dk.lundudvikling.liarsdice.Models.Dice;

public class MainActivity extends AppCompatActivity {
    private FragmentHandler fragmentHandler;
    private final int diceMax = 6;
    private Random rand = new Random();

    private ImageView ivCreateRoom;
    private ImageView ivJoinRoom;
    private ImageView ivBackButton;
    private TextView tvToolbarTitle;

    // TODO: 27-01-2018 Implementer createGame
    // TODO: 27-01-2018 Implementer joinGame
    // TODO: 27-01-2018 Tilføj back button til toolbar som altid popper backstack

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fragmentHandler = new FragmentHandler(this);
        fragmentHandler.startTransactionNoBackStack(new MainFragment());
        initializeToolbar();

        setupClickListeners();

    }

    public void setupClickListeners(){
        ivCreateRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHandler.startTransactionAnimated(new CreateGameFragment(),
                        R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });
        ivJoinRoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHandler.startTransactionAnimated(new JoinGameFragment(),
                        R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            }
        });

        ivBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentHandler.popBackStack();
            }
        });
    }



    public void initializeToolbar(){
        Toolbar mainToolbar = findViewById(R.id.main_toolbar);
        setSupportActionBar(mainToolbar);
        tvToolbarTitle = mainToolbar.findViewById(R.id.toolbar_title);
        ivCreateRoom = mainToolbar.findViewById(R.id.toolbar_create_game);
        ivJoinRoom = mainToolbar.findViewById(R.id.toolbar_join_game);
        ivBackButton = mainToolbar.findViewById(R.id.toolbar_back_button);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
    }
}
