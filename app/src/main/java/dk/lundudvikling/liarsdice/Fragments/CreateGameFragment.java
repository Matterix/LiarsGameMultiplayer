package dk.lundudvikling.liarsdice.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.lundudvikling.liarsdice.Handlers.FragmentHandler;
import dk.lundudvikling.liarsdice.R;

public class CreateGameFragment extends BaseFragment {

    private FragmentHandler fragmentHandler;
    private DatabaseReference mDatabaseRef;

    @BindView(R.id.create_game_et_game_name) EditText etGameName;
    @BindView(R.id.create_game_et_game_password) EditText etGamePassword;
    @BindView(R.id.create_game_et_username) EditText etUsername;
    @BindView(R.id.create_game_et_amount_of_players) EditText etAmountOfPlayers;

    // TODO: 28-01-2018 Benyt custom cardview til players efter hvor mange der skal være med
    // TODO: 28-01-2018 MAX antal spillere understøttet bliver 4

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_create_game, container, false);
        fragmentHandler = new FragmentHandler(getMainActivity());
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
    }

    @OnClick(R.id.create_game_btn_new_game) void onNewGameClicked(){
        String gameName = etGameName.getText().toString();
        String gamePassword = etGamePassword.getText().toString();
        String username = etUsername.getText().toString();
        int amountOfPlayers = Integer.parseInt(etAmountOfPlayers.getText().toString());
        String player = "Player";
        for (int i = 0; i < amountOfPlayers; i++){
            mDatabaseRef.child("Games").child(gameName).child("Players").child(player + String.valueOf(i + 1)).setValue(player + (String.valueOf(i + 1)));
        }
        mDatabaseRef.child("Games").child(gameName).child("Players").child("Player1").setValue(username);
        mDatabaseRef.child("Games").child(gameName).child("Password").setValue(gamePassword);
    }
}
