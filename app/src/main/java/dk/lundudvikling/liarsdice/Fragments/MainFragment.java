package dk.lundudvikling.liarsdice.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import dk.lundudvikling.liarsdice.Handlers.FragmentHandler;
import dk.lundudvikling.liarsdice.R;


public class MainFragment extends BaseFragment {

    private FragmentHandler fragmentHandler;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        fragmentHandler = new FragmentHandler(getMainActivity());
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }
}
