package dk.lundudvikling.liarsdice.Fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.lundudvikling.liarsdice.Handlers.FragmentHandler;
import dk.lundudvikling.liarsdice.Models.Dice;
import dk.lundudvikling.liarsdice.R;

public class GameFragment extends BaseFragment {

    private Bundle args;
    private String gameName;
    private String username;
    private String playerId;
    private DatabaseReference mDatabaseRef;
    private int diceAmount;
    private ArrayList<Dice> playerDices;
    private int diceMax = 6;
    Random rand = new Random();
    @BindView(R.id.fragment_game_tv_title) TextView tvTitle;
    @BindView(R.id.fragment_game_et_amount_of_dice) EditText etAmountOfDice;
    @BindView(R.id.fragment_game_btn_start_game) Button btnStartGame;

    // TODO: 28-01-2018 Ny toolbar når man rammer gamefragment
    private FragmentHandler fragmentHandler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_game, container, false);
        fragmentHandler = new FragmentHandler(getMainActivity());
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        args = getArguments();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
        // TODO: 28-01-2018 Dice objekter her!

        gameName = args.getString("gameName");
        username = args.getString("username");
        playerId = args.getString("player");
        tvTitle.setText(gameName + " " + username + " " + playerId);

    }

    @OnClick(R.id.fragment_game_btn_start_game) void onStartGameClicked(){
        diceAmount = Integer.parseInt(etAmountOfDice.getText().toString());
        playerDices = new ArrayList<>();
        for (int i = 0; i < diceAmount; i++){
            playerDices.add(new Dice(i + 1, rand.nextInt(diceMax) + 1));
        }
        for (Dice dice : playerDices){
            mDatabaseRef.child("Games").child(gameName).child("Dices").child(playerId).child(String.valueOf(dice.diceId)).setValue(dice.diceValue);
        }
    }
}
