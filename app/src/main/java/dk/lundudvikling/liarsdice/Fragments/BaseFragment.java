package dk.lundudvikling.liarsdice.Fragments;

import android.support.v4.app.Fragment;

import dk.lundudvikling.liarsdice.MainActivity;

public class BaseFragment extends Fragment {

    private MainActivity mainActivity;


    public BaseFragment() {
        super();
    }

    public <T> T findViewById(int id) {
        return (T) getView().findViewById(id);
    }

    public MainActivity getMainActivity() {
        if (mainActivity == null)
            mainActivity = (MainActivity) getActivity();
        return mainActivity;
    }

}
