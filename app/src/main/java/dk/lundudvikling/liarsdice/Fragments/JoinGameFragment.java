package dk.lundudvikling.liarsdice.Fragments;

import android.os.Bundle;
import android.service.autofill.Dataset;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dk.lundudvikling.liarsdice.Handlers.FragmentHandler;
import dk.lundudvikling.liarsdice.R;


public class JoinGameFragment extends BaseFragment {

    private FragmentHandler fragmentHandler;
    private DatabaseReference mDatabaseRef;

    @BindView(R.id.join_game_et_game_name) EditText etGameName;
    @BindView(R.id.join_game_et_game_password) EditText etGamePassword;
    @BindView(R.id.join_game_et_username) EditText etUsername;

    private String gameName;
    private String gamePassword;
    private String username;
    private String freeSpot;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_join_game, container, false);
        fragmentHandler = new FragmentHandler(getMainActivity());
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mDatabaseRef = FirebaseDatabase.getInstance().getReference();
    }

    @OnClick(R.id.join_game_btn_new_game)
    void onJoinGameClicked() {
        gameName = etGameName.getText().toString();
        gamePassword = etGamePassword.getText().toString();
        username = etUsername.getText().toString();
        getGame();
    }

    public void getGame() {
        // TODO: 28-01-2018 Tjek password før join

        mDatabaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.child("Games").hasChild(gameName)){

                    for (DataSnapshot snapshot : dataSnapshot.child("Games").child(gameName).child("Players").getChildren()){
                        //Find første ledige plads
                        if (snapshot.getKey().toString().equals(snapshot.getValue().toString())){

                            Log.i("TESTTAG", "LEDIG PLADS");
                            // TODO: 28-01-2018 Indsæt spiller her
                            freeSpot = snapshot.getKey().toString();
                            mDatabaseRef.child("Games").child(gameName).child("Players").child(freeSpot).setValue(username);

                            Bundle args = new Bundle();
                            args.putString("gameName", gameName);
                            args.putString("username", username);
                            args.putString("player", freeSpot);
                            GameFragment gameFragment = new GameFragment();
                            gameFragment.setArguments(args);
                            fragmentHandler.startTransactionWithBackStack(gameFragment);
                            break;
                        }else{
                            // TODO: 28-01-2018 Find et andet sted at melde at spil er fuld!
                            Snackbar.make(getView(), "Spillet er fuldt!", Snackbar.LENGTH_SHORT).show();
                        }
                    }


                }else{
                    Snackbar.make(getView(), "Spil ikke fundet", Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }

        });
    }
}
